import https from 'https'
import _ from 'lodash'

const BTC_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M'
const BTC_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M'
const ETH_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M'
const ETH_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M'

const api = {};

api.getDailyBtc = () => {
	return new Promise ((resolve, reject) => {
		https.get(BTC_DIGITAL_CURRENCY_INTRADAY_URL, (resp) => {
		//simulated error
		if (Math.rand(0, 1) < 0.1) throw new Error('How unfortunate! The API Request Failed')
		
		let data = '';

	  resp.on('data', (chunk) => {
	  	data += chunk;
	  });

	  resp.on('end', () => {
	  	var result = JSON.parse(data)
	  	let by_hours = [];

	  	_.forEach(result["Time Series (Digital Currency Intraday)"], (value, key)=>{
	  		if(_.endsWith(key, "00:00")){
	  			by_hours.push({ time: key, value: value })
	  		}
	  	});

	    resolve(_.slice(by_hours, -24)); //last 24 hours
	  });

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});	
})	
};

api.getMonthlyBtc = () => {
	return new Promise ((resolve, reject) => {
		https.get(BTC_DIGITAL_CURRENCY_MONTHLY_URL, (resp) => {
		//simulated error
		if (Math.rand(0, 1) < 0.1) throw new Error('How unfortunate! The API Request Failed')
		
		let data = '';

	  resp.on('data', (chunk) => {
	  	data += chunk;
	  });

	  resp.on('end', () => {
	  	var result = JSON.parse(data)
	  	let by_months = [];

	  	_.forEach(result["Time Series (Digital Currency Monthly)"], (value, key)=>{
	  			by_months.push({ time: key, value: value })
	  	});

	    resolve(_.slice(by_months, -12)); //last 24 months

	  });

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});	
	})
};


// ETH
api.getDailyEth = () => {
	return new Promise ((resolve, reject) => {
		https.get(ETH_DIGITAL_CURRENCY_INTRADAY_URL, (resp) => {
		//simulated error
		if (Math.rand(0, 1) < 0.1) throw new Error('How unfortunate! The API Request Failed')
		let data = '';

	  resp.on('data', (chunk) => {
	  	data += chunk;
	  });

	  resp.on('end', () => {
	  	var result = JSON.parse(data)
	  	let by_hours = [];

	  	_.forEach(result["Time Series (Digital Currency Intraday)"], (value, key)=>{
	  		if(_.endsWith(key, "00:00")){
	  			by_hours.push({ time: key, value: value })
	  		}
	  	});

	    resolve(_.slice(by_hours, -24)); //last 24 hours
	  });

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});	
})	
};

api.getMonthlyEth = () => {
	return new Promise ((resolve, reject) => {
		https.get(ETH_DIGITAL_CURRENCY_MONTHLY_URL, (resp) => {
		//simulated error
		if (Math.rand(0, 1) < 0.1) throw new Error('How unfortunate! The API Request Failed')
		let data = '';

	  resp.on('data', (chunk) => {
	  	data += chunk;
	  });

	  resp.on('end', () => {
	  	var result = JSON.parse(data)
	  	let by_months = [];

	  	_.forEach(result["Time Series (Digital Currency Monthly)"], (value, key)=>{
	  			by_months.push({ time: key, value: value })
	  	});

	    resolve(_.slice(by_months, -12)); //last 24 months

	  });

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});	
	})
};

export default api