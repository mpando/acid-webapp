import React from 'react';
import {FormattedMessage} from 'react-intl';
import D3 from 'd3'

import BaseComponent from '../../classes/base-component';

import strings from './strings';

class Main extends BaseComponent {
  render() {
    return (
      <main>
        <div id="btc_daily"></div>
        <div id="btc_monthly"></div>
        <div id="eth_daily"></div>
        <div id="eth_monthly"></div>
      </main>
    );
  }
}

export default Main;
