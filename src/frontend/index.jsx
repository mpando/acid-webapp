/**
 * @file Main module: Renders the complete app to the screen
 * @module index
 */

// Global style. Import it first so its in the DOM first.
import './styles/index.styl';

import React from 'react';
import ReactDOM from 'react-dom';
import { LineChart } from 'react-d3'
import api from '../backend/modules/api'
import _ from 'lodash'
import moment from 'moment'

//API Daily BTC
api.getDailyBtc().then((result) => {
	let clp_data = [];
	let usd_data = [];

	_.forEach(result, (value, key) => {
		clp_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["1a. price (CLP)"]) })
		usd_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["1b. price (USD)"]) })
	});

	var DEMO = React.createClass({
		render: function render() {
			return React.createElement('div',{ className: 'btc-daily'}, React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'Pesos chilenos',
					values: clp_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Bitcoin Daily CLP (BTC)',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}),
			React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'USD',
					values: usd_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Bitcoin Daily USD (BTC)',
				yAxisLabel: 'Valor',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}));

		}
	});

	ReactDOM.render(
		React.createElement(DEMO, null),
		document.getElementById('btc_daily')
		);
});

//API Monthly BTC
api.getMonthlyBtc().then((result) => {
	let clp_data = [];
	let usd_data = [];

	_.forEach(result, (value, key) => {
		clp_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["4a. close (CLP)"]) })
		usd_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["4b. close (USD)"]) })
	});

	var DEMO = React.createClass({
		render: function render() {
			return React.createElement('div',{ className: 'btc-monthly'}, React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'Pesos chilenos',
					values: clp_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Bitcoin Monthly CLP (BTC)',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}),
			React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'USD',
					values: usd_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Bitcoin Monthly USD (BTC)',
				yAxisLabel: 'Valor',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}));

		}
	});

	ReactDOM.render(
		React.createElement(DEMO, null),
		document.getElementById('btc_monthly')
		);
});


//API Daily ETH
api.getDailyEth().then((result) => {
	let clp_data = [];
	let usd_data = [];

	_.forEach(result, (value, key) => {
		clp_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["1a. price (CLP)"]) })
		usd_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["1b. price (USD)"]) })
	});

	var DEMO = React.createClass({
		render: function render() {
			return React.createElement('div',{ className: 'btc-daily'}, React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'Pesos chilenos',
					values: clp_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Ethereum Daily CLP (BTC)',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}),
			React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'USD',
					values: usd_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Ethereum Daily USD (BTC)',
				yAxisLabel: 'Valor',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}));

		}
	});

	ReactDOM.render(
		React.createElement(DEMO, null),
		document.getElementById('eth_daily')
		);
});

//API Monthly Eth
api.getMonthlyEth().then((result) => {
	let clp_data = [];
	let usd_data = [];

	_.forEach(result, (value, key) => {
		clp_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["4a. close (CLP)"]) })
		usd_data.push({x: moment(value.time).toDate(), y: _.parseInt(value.value["4b. close (USD)"]) })
	});

	var DEMO = React.createClass({
		render: function render() {
			return React.createElement('div',{ className: 'btc-monthly'}, React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'Pesos chilenos',
					values: clp_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Ethereum Monthly CLP (BTC)',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}),
			React.createElement(LineChart, {
				legend: true,
				data: [{
					name: 'USD',
					values: usd_data,
					strokeWidth: 3,
					strokeDashArray: "5,5"
				}],
				width: 900,
				height: 400,
				viewBoxObject: {
					x: -100,
					y: 0,
					width: 900,
					height: 400
				},
				title: 'Ethereum Monthly USD (BTC)',
				yAxisLabel: 'Valor',
				xAxisLabel: 'Tiempo transcurrido (hora)',
				gridHorizontal: true
			}));

		}
	});

	ReactDOM.render(
		React.createElement(DEMO, null),
		document.getElementById('eth_monthly')
		);
});

import Main from './modules/main';
// import LineDataSeries from './modules/d3/LineDataSeries'


// if (!global.Intl) {
//   require('intl');
//   require('intl/locale-data/jsonp/en');
// }

// import {addLocaleData, IntlProvider} from 'react-intl';
// import enLocaleData from 'react-intl/locale-data/en';
// addLocaleData(enLocaleData);

// import strings from './locale/en/strings.json';

// Render the application component to the proper DOM element.
ReactDOM.render(
  <Main />,
  document.getElementById('root')
);


